package common_utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener{
	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
    ExtentTest test;

    public void reportConfigurations()
    {
    	sparkReporter= new ExtentSparkReporter(".\\extent-report\\report.html");
    	extentReport=new ExtentReports();
    	extentReport.attachReporter(sparkReporter);
    	extentReport.setSystemInfo("OS", "Windows11");
    	extentReport.setSystemInfo("User", "Rajshekhar");
    	sparkReporter.config().setDocumentTitle("RestAssured Extent Listener Report");
    	sparkReporter.config().setReportName("1st Extent reprot");
    	sparkReporter.config().setTheme(Theme.DARK);
    	
    }
    public void onStart(ITestContext result)
{
	reportConfigurations();
    	System.out.println("On start method invoked");
}
public void onFinish(ITestContext result)
{
	System.out.println("On finish method invoked");
	extentReport.flush();
}
public void onTestFailure(ITestResult result)
{
	System.out.println("name of test failed"+result.getName());
	test=extentReport.createTest(result.getName());
	test.log(Status.FAIL, MarkupHelper.createLabel("Name of failed case is "+result.getName(), ExtentColor.RED));
}

public void onTestSkipped(ITestResult result)
{
	System.out.println("On test skipped method invoked"+result.getName());
	test=extentReport.createTest(result.getName());
	test.log(Status.SKIP, MarkupHelper.createLabel("Name of skiped case is "+result.getName(), ExtentColor.YELLOW));
}
public void onTestStart(ITestResult result)
{
	System.out.println("On test start method invoked"+result.getName());
}

public void onTestSuccess(ITestResult result)
{
	System.out.println("name of test method executed successfully"+result.getName());
	test=extentReport.createTest(result.getName());
	test.log(Status.PASS, MarkupHelper.createLabel("Name of failed case is "+result.getName(), ExtentColor.GREEN));
}
public void onTestFailedButWithinSuccessPercentage(ITestResult result)
{
	
}


}
