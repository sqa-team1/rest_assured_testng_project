Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "raj",
    "job": "qa"
}

Response header date is : 
Thu, 07 Mar 2024 15:24:12 GMT

Response body is : 
{"name":"raj","job":"qa","id":"888","createdAt":"2024-03-07T15:24:12.761Z"}