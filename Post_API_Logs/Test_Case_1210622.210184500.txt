Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "suhas",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 15:36:20 GMT

Response body is : 
{"name":"suhas","job":"leader","id":"224","createdAt":"2024-03-07T15:36:20.478Z"}