Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "suhas",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 17:12:14 GMT

Response body is : 
{"name":"suhas","job":"leader","id":"595","createdAt":"2024-03-07T17:12:14.004Z"}