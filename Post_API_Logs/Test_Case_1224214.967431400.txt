Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Suhas",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 17:12:12 GMT

Response body is : 
{"name":"Suhas","job":"leader","id":"714","createdAt":"2024-03-07T17:12:12.691Z"}